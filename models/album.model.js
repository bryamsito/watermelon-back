'use strict'

let mongoose = require('mongoose');

let Schema = mongoose.Schema;

let AlbumSchema = Schema({
    name:String,
    results: []
})

module.exports = mongoose.model('Album', AlbumSchema);