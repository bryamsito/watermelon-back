'use strict'

let express = require('express');
let albumController = require('../controllers/album.controller');

let router = express.Router();


router.get('/get-album/:name',albumController.getAlbum);


module.exports = router;