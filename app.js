'use strict'

let express = require('express');
let bodyParser = require('body-parser');



let app = express();

//rutas
let albumRoutes = require('./routes/album');
//middlewares
app.use(bodyParser.urlencoded({extended:false}));
app.use(bodyParser.json());
app.use('/',albumRoutes)


module.exports = app;

