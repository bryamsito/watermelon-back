'use strict'

const axios = require('axios');
const qs = require('querystring');
const env = require('../env');
let AlbumModel = require('../models/album.model');


let controller = {

    getAlbum: async (req,res) => {

        try{
            
            let data = await getAccessToken()
                                .then((response) => {
                                    return response.data
                                })
                                .catch(function (error) {
                                    console.log(error);
                                });
            let access_token = data.access_token;    
            let albums = await getAlbums(access_token, req.params.name)
                                .then((response)=>{
                                    return response.data.albums.items
                                });
            const search = await AlbumModel.create({
                name: req.params.name,
                results: albums
            })
            return res.status(200).send({
                message: "Operacion exitosa",
                data: search
            });
        }catch(err){
            console.log(err);
        }

        
    },
};

async function getAccessToken(){

    const formData = env.sptfy.credentials;
    const options = {
        method: 'POST',
        headers: { 'content-type': 'application/x-www-form-urlencoded' },
        data: qs.stringify(formData),
        url: env.sptfy.url_token,
    };

    return await axios(options);
}

async function getAlbums(access_token,nameAlbum){

    const options = {
        method: 'GET',
        headers: { 'Authorization': "Bearer "+access_token },
        url: env.sptfy.url_album+nameAlbum+"&type=album"
    };
    return axios(options);
}

module.exports = controller;