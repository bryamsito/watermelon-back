'use strict'

let mongoose = require('mongoose');
let app = require('./app');

const PORT = 8000;


mongoose.Promise = global.Promise;
mongoose.connect('mongodb://watermelon:watermelon1@ds050077.mlab.com:50077/spotify_watermelon')
        .then(() => {
            console.log("conexion exitosa");

            app.listen(PORT, () => {
                console.log("server up");
            });
        });